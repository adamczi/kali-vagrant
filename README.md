# kali-vagrant

Custom Vagrantfile for Kali

Boots up latest Kali-rolling instance with GUI and custom `tmux` and `zsh` scripts.

Network is NATted by default, you can change it in Vagrantfile.
